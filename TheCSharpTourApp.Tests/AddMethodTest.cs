using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TheCSharpTourApp.Tests
{
    [TestClass]
    public class AddMethodTest
    {
        [TestMethod]
        public void AddMethodShouldAddTwoNumbers()
        {
            //Arrange
            int first = 10;
            int second = 23;

            Assert.AreEqual(33, Functions.Add(first, second));
        }
    }
}
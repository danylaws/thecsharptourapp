﻿using System;

namespace TheCSharpTourApp
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            Console.WriteLine("Ce programme additionne deux nombres");

            Console.Write("premier nombre : ");

            var firstNumber = Console.ReadLine();

            var first = int.Parse(firstNumber);

            Console.Write("deuxième nombre : ");

            string secondNumber = Console.ReadLine();

            var second = int.Parse(secondNumber);

            Console.WriteLine("Résultat : " + Functions.Add(first, second));

            Console.ReadLine();
        }
    }
}